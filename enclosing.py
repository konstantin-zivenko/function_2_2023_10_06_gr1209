def power_factory(exp):
    def power(base):
        return base ** exp

    return power
