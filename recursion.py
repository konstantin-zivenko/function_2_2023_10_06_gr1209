#  предки(ви) = (ваші_батьки) + (предки(ваших батьків))

def countdown(n: int) -> None:
    print(n)
    if n == 0:
        return
    else:
        countdown(n - 1)


def countdown_2(n):
    print(n)
    if n > 0:
        countdown_2(n - 1)


def countdown_non_recursive(n):
    while n >= 0:
        print(n)
        n -= 1


def factorial(n: int) -> int:
    # print(f"factorial() called with n = {n}")
    result = 1 if n < 2 else n * factorial(n - 1)
    # print(f"-------->  factorial({n}) returns {result}")
    return result


def factorial_without_recursion(n: int) -> int:
    return_value = 1
    for i in range(2, n + 1):
        return_value *= i

    return return_value


if __name__ == "__main__":
    print(factorial(5))

